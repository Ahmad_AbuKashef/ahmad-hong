package com.arapeak.lalamovechallenge.utilis;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.net.ConnectivityManager;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.ImageView;

import com.android.volley.ui.PhotoView;

import com.arapeak.lalamovechallenge.R;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by Alaa Sami on 2/24/2018.
 */

public class Utills {



  public static void setPhoto(Context context, ImageView imageView, String url) {
    try {
      if (url != null) {
        if (!url.isEmpty()) {
          Picasso.with(context)
              .load(url)
              .error(R.drawable.ic_launcher_background)
              .into(imageView);

        } else {
          Picasso.with(context)
              .load(R.drawable.ic_launcher_background)
              .placeholder(R.drawable.ic_launcher_background)
              .into(imageView);
        }
      } else {
        Picasso.with(context)
            .load(R.drawable.ic_launcher_background)
            .placeholder(R.drawable.ic_launcher_background)
            .into(imageView);
      }

    } catch (Exception e) {
      e.printStackTrace();
    }

  }

  public static void setPhoto(Context context, CircleImageView imageView, String url) {
    try {
      if (url != null) {
        if (!url.isEmpty()) {
          Picasso.with(context)
              .load(url)
//              .placeholder(R.drawable.loader_rotate)
              .resize(50, 50)
              .error(R.drawable.ic_launcher_background)
              .into(imageView);

        } else {
          Picasso.with(context)
              .load(R.drawable.ic_launcher_background)
//              .placeholder(R.drawable.logo)
              .into(imageView);
        }
      } else {
        Picasso.with(context)
            .load(R.drawable.ic_launcher_background)
//            .placeholder(R.drawable.logo)
            .into(imageView);
      }

    } catch (Exception e) {
      e.printStackTrace();
    }

  }

  public static void setPhoto(Context context, PhotoView imageView, String url) {
    try {
      if (url != null) {
        if (!url.isEmpty()) {
          Picasso.with(context)
              .load(url)
//              .placeholder(R.drawable.loader_rotate)
              .resize(50, 50)
              .error(R.drawable.ic_launcher_background)
              .into(imageView);

        } else {
          Picasso.with(context)
              .load(R.drawable.ic_launcher_background)
//              .placeholder(R.drawable.logo)
              .into(imageView);
        }
      } else {
        Picasso.with(context)
            .load(R.drawable.ic_launcher_background)
//            .placeholder(R.drawable.logo)
            .into(imageView);
      }

    } catch (Exception e) {
      e.printStackTrace();
    }

  }


  public static boolean isNetworkAvailable(final Context context) {
    final ConnectivityManager connectivityManager = ((ConnectivityManager) context
        .getSystemService(Context.CONNECTIVITY_SERVICE));
    return connectivityManager.getActiveNetworkInfo() != null && connectivityManager
        .getActiveNetworkInfo().isConnected();
  }


  public static File getImage(Bitmap image) {
    try {
      File imgFile = new File(
          Environment.getExternalStorageDirectory() + File.separator + "image_");
      imgFile.createNewFile();
      ByteArrayOutputStream bos = new ByteArrayOutputStream();
      Log.i("bytes size ", bos.size() + "");
      image.compress(CompressFormat.JPEG, 50 /*ignored for PNG*/, bos);
      byte[] bitmapdata = bos.toByteArray();
      FileOutputStream fos = new FileOutputStream(imgFile);
      fos.write(bitmapdata);
      fos.flush();
      fos.close();
      return imgFile;
    } catch (IOException e) {
      e.printStackTrace();
      Log.i("getImage: ", "ERROR");
      return null;
    }
  }

  public static int dpValue(float value, Context context) {
    DisplayMetrics metrics = context.getResources().getDisplayMetrics();
    float dp = value;
    float fpixels = metrics.density * dp;
    int pixels = (int) (fpixels + 0.5f);
    return pixels;
  }

}
