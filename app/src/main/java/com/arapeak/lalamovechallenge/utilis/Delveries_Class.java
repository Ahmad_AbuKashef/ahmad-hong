package com.arapeak.lalamovechallenge.utilis;



import java.io.Serializable;

public class  Delveries_Class implements Serializable {



    public  Location location;

    public  String imageUrl;

    public  String description;

    public  int id;

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public class Location implements Serializable {

        public  String address;

        public  double lng;

        public  double lat;

        public  String getAddress() {
            return address;
        }

        public  void setAddress(String address) {
            this.address = address;
        }

        public double getLng() {
            return lng;
        }

        public void setLng(double lng) {
            this.lng = lng;
        }

        public double getLat() {
            return lat;
        }

        public void setLat(double lat) {
            this.lat = lat;
        }
    }
}
