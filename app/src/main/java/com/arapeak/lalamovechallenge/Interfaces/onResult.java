package com.arapeak.lalamovechallenge.Interfaces;

import org.json.JSONException;

import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by Alaa Sami on 2/12/2018.
 */

 public interface onResult {

 void onSuccess(Object object) throws IOException, JSONException;
 void onFail(Object object);

}
