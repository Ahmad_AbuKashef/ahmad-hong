package com.arapeak.lalamovechallenge.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.arapeak.lalamovechallenge.R;
import com.arapeak.lalamovechallenge.utilis.CircleImageView;
import com.arapeak.lalamovechallenge.utilis.MapWrapperLayout;
import com.arapeak.lalamovechallenge.utilis.Utills;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private MapWrapperLayout mapWrapperLayout;
    private LinearLayout toDetails;
    private CircleImageView sPhoto;
    private TextView desc;
     String description , img_url ;
     double lat , lng ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        Bundle extras = getIntent().getExtras();
        Intent g = getIntent();
        description = extras.getString("desc");
        img_url = extras.getString("img");
        lat = extras.getDouble("lat",0);
        lng = extras.getDouble("lng",0);
        initView();
        desc.setText(description);
        Utills.setPhoto(this, sPhoto, img_url);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        LatLng deliver = new LatLng(lat, lng);
        mMap.addMarker(new MarkerOptions().position(deliver).title("Deliver here"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(deliver));
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(15);
        googleMap.animateCamera(zoom);
    }

    private void initView() {
        mapWrapperLayout = findViewById(R.id.mapWrapperLayout);
        toDetails = findViewById(R.id.to_details);
        sPhoto = findViewById(R.id.s_photo);
        desc = findViewById(R.id.desc);
    }
}
