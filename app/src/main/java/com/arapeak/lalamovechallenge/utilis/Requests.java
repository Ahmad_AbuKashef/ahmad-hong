package com.arapeak.lalamovechallenge.utilis;

import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.arapeak.lalamovechallenge.Interfaces.onResult;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class Requests {

//    public static void Delevries_List(String endpoint, final Context context,
//                                 final onResult onResult) {
//
//        RequestQueue queue = Volley.newRequestQueue(context);
////
//        StringRequest stringRequest = new StringRequest(Request.Method.GET, endpoint,
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//                        Log.d("RESPONSE", response);
//                        Gson mGson = new Gson();
//                        Type listType = new TypeToken<List<Delveries_Class>>() {
//                        }.getType();
//
//                        ArrayList<Delveries_Class> table;
//                        table = mGson.fromJson(response.toString(), listType);
////              table.notify();
//                        onResult.onSuccess(table);
//
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Log.d("TAG", "Error " + error.getMessage());
//
//                    onResult.onFail(error);
//
//            }
//        });
//
//        int socketTimeout = 60000;//30 seconds - change to what you want
//        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
//                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//        stringRequest.setRetryPolicy(policy);
//        stringRequest.setShouldCache(false);
//        queue.add(stringRequest);
//
//    }

    public static void Delevries_List(final String endpoint, final Context context, final onResult onResult) {
        Log.d("Delevries_List", "");

        RequestQueue queue = Volley.newRequestQueue(context);
        JsonArrayRequest stringRequest = new JsonArrayRequest(Request.Method.GET, endpoint, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d("RESPONSE", response.toString());

                        Gson mGson = new Gson();
                        Type listType = new TypeToken<List<Delveries_Class>>() {
                        }.getType();

                        ArrayList<Delveries_Class> table;
                        table = mGson.fromJson(response.toString(), listType);
                        try {
                            onResult.onSuccess(table);
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }

        }) ;

        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        stringRequest.setShouldCache(false);
        queue.add(stringRequest);
    }
}
