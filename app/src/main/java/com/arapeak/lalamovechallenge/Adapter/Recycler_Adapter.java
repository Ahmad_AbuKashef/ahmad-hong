package com.arapeak.lalamovechallenge.Adapter;

import android.app.Activity;
import android.app.Dialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.arapeak.lalamovechallenge.utilis.Delveries_Class;
import com.arapeak.lalamovechallenge.Interfaces.AdapterCallback;
import com.arapeak.lalamovechallenge.R;
import com.arapeak.lalamovechallenge.utilis.Utills;

import java.util.List;


public class Recycler_Adapter extends
    RecyclerView.Adapter<Recycler_Adapter.TableViewHolder> {

  private List<Delveries_Class> Dataset;
  private static Activity context;
  Dialog loadingDialog;
  AdapterCallback mCallback;
  boolean showButtons;
  ProgressBar progressBar;

  public Recycler_Adapter(Activity baseContext, List<Delveries_Class> data,
                          AdapterCallback mCallback) {

    context = baseContext;
    Dataset = data;
    this.mCallback = mCallback;
  }


  @Override
  public TableViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.delivery_list_item, parent, false);
    return new TableViewHolder(view);
  }

  @Override
  public void onBindViewHolder(final TableViewHolder holder, final int position) {

    holder.desc.setText(Dataset.get(position).getDescription());
    Utills.setPhoto(context, holder.photo, Dataset.get(position).getImageUrl());

    holder.toDetails.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        mCallback.onItemClick(holder.getAdapterPosition());
      }
    });

  }

  @Override
  public int getItemCount() {
    return Dataset.size();
  }

  public class TableViewHolder extends RecyclerView.ViewHolder {

    TextView desc ;
    ImageView photo;
    LinearLayout toDetails ;


    public TableViewHolder(View itemView) {
      super(itemView);

      photo =itemView.findViewById(R.id.s_photo);
      desc =  itemView.findViewById(R.id.desc);
      toDetails =  itemView.findViewById(R.id.to_details);


    }


  }


}
