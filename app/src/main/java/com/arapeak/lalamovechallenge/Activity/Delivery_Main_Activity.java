package com.arapeak.lalamovechallenge.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.arapeak.lalamovechallenge.Adapter.Recycler_Adapter;
import com.arapeak.lalamovechallenge.utilis.Delveries_Class;
import com.arapeak.lalamovechallenge.Interfaces.AdapterCallback;
import com.arapeak.lalamovechallenge.Interfaces.onResult;
import com.arapeak.lalamovechallenge.R;
import com.arapeak.lalamovechallenge.utilis.Requests;
import com.arapeak.lalamovechallenge.utilis.Utills;
import com.cooltechworks.views.shimmer.ShimmerRecyclerView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import static com.arapeak.lalamovechallenge.utilis.Config_EndPoints.BASE_URL;


public class Delivery_Main_Activity extends Activity implements AdapterCallback{

    private ShimmerRecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    public ArrayList<Delveries_Class> data = new ArrayList<>();
    private String nextURL;
    private boolean isloading = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery);
        initView();
        if(Utills.isNetworkAvailable(this)){
            getChatList();
        }else {
            ObjectInputStream in = null;
            try {
                in = new ObjectInputStream(new FileInputStream(new File(new File(getCacheDir(),"")+"cacheFile.srl")));
                Object jsonObject =in.readObject();
                data.addAll((ArrayList<Delveries_Class>) jsonObject);
                initRecycler();
                recyclerView.hideShimmerAdapter();
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

        }


//        Toast.makeText(this, ""+Utills.isNetworkAvailable(this), Toast.LENGTH_SHORT).show();

    }

    private void initView() {
        recyclerView = findViewById(R.id.recyclerView);
    }

    private void initRecycler() {
        mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new Recycler_Adapter(Delivery_Main_Activity.this, data, this);
        recyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
        isloading = false;

    }

    @Override
    public void onItemClick(int position) {
        Intent to_details =new Intent(Delivery_Main_Activity.this,MapsActivity.class);
        to_details.putExtra("img",data.get(position).getImageUrl());
        to_details.putExtra("lat",data.get(position).getLocation().getLat());
        to_details.putExtra("lng",data.get(position).getLocation().getLng());
        to_details.putExtra("desc",data.get(position).getDescription());

        startActivity(to_details);

    }

    private void getChatList() {
        recyclerView.showShimmerAdapter();
        String URL = BASE_URL;
        Requests.Delevries_List(URL, Delivery_Main_Activity.this, new onResult() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onSuccess(Object object)  {
                // Instantiate a JSON object from the request response
                data.addAll((ArrayList<Delveries_Class>) object);
                initRecycler();
                recyclerView.hideShimmerAdapter();
// Save the JSONOvject
                ObjectOutput out = null;
                try {
//                    ArrayList jsonObject = new ArrayList((ArrayList<Delveries_Class>)object);
                    out = new ObjectOutputStream(new FileOutputStream(new File(getCacheDir(),"")+"cacheFile.srl"));
                    out.writeObject( object );
                    out.close();

                } catch (IOException e) {
                    e.printStackTrace();
                }



            }

            @Override
            public void onFail(Object object) {
                recyclerView.hideShimmerAdapter();
            }
        });

    }

}
